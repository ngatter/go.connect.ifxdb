package ifxdb

import (
	_ "github.com/alexbrainman/odbc"
	"runtime"
	"database/sql"
)

var (
    driver  string
	host    string
	star    string
    dbc     string
    port    string
    user    string
    pwd     string
	DSNIFX  string
)

func PrepConn(ent string) {
    /* Armo el String de coneccion */
    if runtime.GOARCH == "386" {
	    driver = "IBM INFORMIX ODBC DRIVER"    /* Nombre del Driver */
	}else {
		driver = "IBM INFORMIX ODBC DRIVER (64-bit)"    /* Nombre del Driver */	
	}  
	if ent == "PRD" {
		host = "10.4.100.6"             /* IP/Nombre Server  */
		star = "carsdb"                 /* Star de la base */
	    dbc  = "megatone"               /* Nombre de la Base de Datos */
    	port = "1550"                   /* Port number */
	    user = "ngatter"                /* user login : you should do in a safer way ! */
		pwd  = "nestor"                 /* user password : you should do in a safer way ! */
	}
	if ent == "QAS"	{
		host = "10.4.100.152"             /* IP/Nombre Server  */
		star = "carip2db"                 /* Star de la base */
	    dbc  = "megatone"               /* Nombre de la Base de Datos */
    	port = "1550"                   /* Port number */
	    user = "ngatter"                /* user login : you should do in a safer way ! */
		pwd  = "nestor"                 /* user password : you should do in a safer way ! */
	}

	DSNIFX = "DRIVER=" + driver + ";HOST=" + host + ";SRVR=" + star + ";SERV=" + port + ";UID=" + user + ";PWD=" + pwd + ";DATABASE=" + dbc + ";DLOC=en_US.819;CLOC=en_US.CP1252;PRO=olsoctcp;"            
}

func GetConnect() string {
	return DSNIFX
}

func CreateConnect(p string) (*sql.DB, error) {
	PrepConn(p) 
	db, err := sql.Open("odbc", DSNIFX)
	return db, err
}

func Init() { 
	
	// Connect
	PrepConn("QAS")	
	//log.Printf("DSN (connection string) : %s\n", DSN)
	//db, err := sql.Open("odbc", DSN)
	//if err != nil {
	//	log.Fatal(err)
	//}
}	

